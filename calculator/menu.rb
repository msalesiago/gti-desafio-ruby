require_relative 'operations'

module Calculator
  class Menu
    def initialize
    operation = Operations.new

    puts"|**************************************|"
    puts"| Bem vindo a calculadora maluca       |"
    puts"|**************************************|"
    puts"|                                      |"
    puts"| 1 - Operação média preconceituosa    |"
    puts"| 2 - Operação calculadora sem números |"
    puts"| 3 - Operação filtro de filmes        |"
    puts"| 0 - Sair                             |"
    puts"|                                      |"
    puts"|**************************************|"

    print"Sua opção é:"
    opcao = gets.chomp.to_i
    system "clear || cls"

    case opcao
      when 1
        puts" Opção média preconceituosa "
        puts" Insira a o JSON com as notas"
        grades = gets.chomp
        puts" Digite a lista de alunos a serem ignorados: "
        blacklist = gets.chomp

        puts"A média final será: #{operation.biased_mean(grades, blacklist)}"
       
        puts"Aperte enter para continuar"
        gets.chomp
        system "clear || cls"
      
      when 2
        puts" opção calculadora sem números"
        puts" Insira os números desejados para saber se os mesmos são divisívies por 25"
        numbers = gets.chomp

        puts"#{operation.no_integers(numbers)}"

        puts"Aperte enter para continuar"
        gets.chomp
        system "clear || cls"

      when 3 
        puts" Opção filtrar filmes "
        puts" Insira os gêneros dos filmes: "
        genres = gets.chomp
        puts" Insira o ano dos filmes "
        year = gets.chomp
        puts"#{operation.filter_films(genres, year)}"

        puts"Aperte enter para continuar"
        gets.chomp
        system "clear || cls"

      when 0
        exit

      end
    end
  end
end