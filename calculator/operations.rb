require_relative '../extra_operations'
require 'net/http'
require 'json'

module Calculator
  class Operations
    include ExtraOperations
  
    def biased_mean(grades, blacklist)
      grades = JSON.parse(grades)
      blacklist = blacklist.split(" ")

      sum = 0
      student = 0

      grades.each do |name, grade|
        if not blacklist.incude?(name)
          sum += grade
          student += 1 
        end
      end

      return (sum/student).to_f
    end
  
    def no_integers(numbers)
      numbers = numbers.split(" ")
      numbersDivi = ["00","25","50","75"]
      resp = " "
      
      numbers.each do |number|
        if number == "00"
          resp += "No "
        elsif numbersDivi.include?(number[-2..-1])
          resp += "Yes "
        else
          resp += "No "
        end
      end
      return resp
    end
  
    def filter_films(genres, year)
      films = get_films[:movies]
      genres = genres.split(" ")
      resp = []
      
      films.each do |mo|
        if(mo[:year] >= year && (genres & mo[:genres]) == genres)
          resp.push(mo[:title])
        end
      end
      return resp  
    end
    
    private
  
    def get_films
      url = 'https://raw.githubusercontent.com/yegor-sytnyk/movies-list/master/db.json'
      uri = URI(url)
      response = Net::HTTP.get(uri)
      return JSON.parse(response, symbolize_names: true)
    end
  end
end
